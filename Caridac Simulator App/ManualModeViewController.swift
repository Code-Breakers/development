//
//  ManualModeViewController.swift
//  Caridac Simulator App
//
//  Created by chandrasekhar vasamsetti on 3/18/16.
//  Copyright © 2016 Kancharla,Venkata Sravya. All rights reserved.
//


import UIKit
// view controller for manual mode
class ManualModeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate {
    
    // to store the instructions
    var instructions:[String] = []
    // to store instructions to display in the instructionqueue table
    var instructionsStore:[String] = []
    var memorycellTF:[UITextField] = []
    // outlet of the UIScrollView
    @IBOutlet var scrollView: UIScrollView!
    // accumulator test
    @IBOutlet weak var accumulatorTF: UITextField!
    // textfield is used to store the program counter value
    @IBOutlet weak var pcTF: UITextField!
    // textfield to take the instructions
    @IBOutlet weak var instructionInputTF: UITextField!
    // decoder1 to display the decoded instruction
    @IBOutlet weak var decoder1TF: UITextField!
    // to display memory address1 cell
    @IBOutlet weak var decoder2TF: UITextField!
    // to display memory address1 cell
    @IBOutlet weak var decoder3TF: UITextField!
    // to display decoded instruction
    @IBOutlet weak var decoder4TF: UITextField!
    // outlet for opcode
    @IBOutlet weak var opcodeSlider: UISlider!
    // outlet for memory address1 slider
    @IBOutlet weak var memoryAddress1Slider: UISlider!
    // outlet for memory address1 slider
    @IBOutlet weak var memoryAddress2Slider: UISlider!
    // outlet for opcode instruction
    @IBOutlet weak var opCodeInstructionTF: UITextField!
    // outlet for memorycellAddress1TF
    @IBOutlet weak var memorycellAddress1TF: UITextField!
    // outlet for memorycellAddress2TF
    @IBOutlet weak var memorycellAddress2TF: UITextField!
    // outlet for output
    @IBOutlet weak var outputTextView: UITextView!
    // outlet for accumulator test value
    @IBOutlet weak var accumulatorTestTF: UITextField!
    // outlets for memory cell textfields
    @IBOutlet weak var memoryTF0:UITextField!
    @IBOutlet weak var memoryTF1:UITextField!
    @IBOutlet weak var memoryTF2:UITextField!
    @IBOutlet weak var memoryTF3:UITextField!
    @IBOutlet weak var memoryTF4:UITextField!
    @IBOutlet weak var memoryTF5:UITextField!
    @IBOutlet weak var memoryTF6:UITextField!
    @IBOutlet weak var memoryTF7:UITextField!
    @IBOutlet weak var memoryTF8:UITextField!
    @IBOutlet weak var memoryTF9:UITextField!
    @IBOutlet weak var memoryTF10:UITextField!
    @IBOutlet weak var memoryTF11:UITextField!
    @IBOutlet weak var memoryTF12:UITextField!
    @IBOutlet weak var memoryTF13:UITextField!
    @IBOutlet weak var memoryTF14:UITextField!
    @IBOutlet weak var memoryTF15:UITextField!
    @IBOutlet weak var memoryTF16:UITextField!
    @IBOutlet weak var memoryTF17:UITextField!
    @IBOutlet weak var memoryTF18:UITextField!
    @IBOutlet weak var memoryTF19:UITextField!
    @IBOutlet weak var memoryTF20:UITextField!
    @IBOutlet weak var memoryTF21:UITextField!
    @IBOutlet weak var memoryTF22:UITextField!
    @IBOutlet weak var memoryTF23:UITextField!
    @IBOutlet weak var memoryTF24:UITextField!
    @IBOutlet weak var memoryTF25:UITextField!
    @IBOutlet weak var memoryTF26:UITextField!
    @IBOutlet weak var memoryTF27:UITextField!
    @IBOutlet weak var memoryTF28:UITextField!
    @IBOutlet weak var memoryTF29:UITextField!
    @IBOutlet weak var memoryTF30:UITextField!
    @IBOutlet weak var memoryTF31:UITextField!
    @IBOutlet weak var memoryTF32:UITextField!
    @IBOutlet weak var memoryTF33:UITextField!
    @IBOutlet weak var memoryTF34:UITextField!
    @IBOutlet weak var memoryTF35:UITextField!
    @IBOutlet weak var memoryTF36:UITextField!
    @IBOutlet weak var memoryTF37:UITextField!
    @IBOutlet weak var memoryTF38:UITextField!
    @IBOutlet weak var memoryTF39:UITextField!
    @IBOutlet weak var memoryTF40:UITextField!
    @IBOutlet weak var memoryTF41:UITextField!
    @IBOutlet weak var memoryTF42:UITextField!
    @IBOutlet weak var memoryTF43:UITextField!
    @IBOutlet weak var memoryTF44:UITextField!
    @IBOutlet weak var memoryTF45:UITextField!
    @IBOutlet weak var memoryTF46:UITextField!
    @IBOutlet weak var memoryTF47:UITextField!
    @IBOutlet weak var memoryTF48:UITextField!
    @IBOutlet weak var memoryTF49:UITextField!
    @IBOutlet weak var memoryTF50:UITextField!
    @IBOutlet weak var memoryTF51:UITextField!
    @IBOutlet weak var memoryTF52:UITextField!
    @IBOutlet weak var memoryTF53:UITextField!
    @IBOutlet weak var memoryTF54:UITextField!
    @IBOutlet weak var memoryTF55:UITextField!
    @IBOutlet weak var memoryTF56:UITextField!
    @IBOutlet weak var memoryTF57:UITextField!
    @IBOutlet weak var memoryTF58:UITextField!
    @IBOutlet weak var memoryTF59:UITextField!
    @IBOutlet weak var memoryTF60:UITextField!
    @IBOutlet weak var memoryTF61:UITextField!
    @IBOutlet weak var memoryTF62:UITextField!
    @IBOutlet weak var memoryTF63:UITextField!
    @IBOutlet weak var memoryTF64:UITextField!
    @IBOutlet weak var memoryTF65:UITextField!
    @IBOutlet weak var memoryTF66:UITextField!
    @IBOutlet weak var memoryTF67:UITextField!
    @IBOutlet weak var memoryTF68:UITextField!
    @IBOutlet weak var memoryTF69:UITextField!
    @IBOutlet weak var memoryTF70:UITextField!
    @IBOutlet weak var memoryTF71:UITextField!
    @IBOutlet weak var memoryTF72:UITextField!
    @IBOutlet weak var memoryTF73:UITextField!
    @IBOutlet weak var memoryTF74:UITextField!
    @IBOutlet weak var memoryTF75:UITextField!
    @IBOutlet weak var memoryTF76:UITextField!
    @IBOutlet weak var memoryTF77:UITextField!
    @IBOutlet weak var memoryTF78:UITextField!
    @IBOutlet weak var memoryTF79:UITextField!
    @IBOutlet weak var memoryTF80:UITextField!
    @IBOutlet weak var memoryTF81:UITextField!
    @IBOutlet weak var memoryTF82:UITextField!
    @IBOutlet weak var memoryTF83:UITextField!
    @IBOutlet weak var memoryTF84:UITextField!
    @IBOutlet weak var memoryTF85:UITextField!
    @IBOutlet weak var memoryTF86:UITextField!
    @IBOutlet weak var memoryTF87:UITextField!
    @IBOutlet weak var memoryTF88:UITextField!
    @IBOutlet weak var memoryTF89:UITextField!
    @IBOutlet weak var memoryTF90:UITextField!
    @IBOutlet weak var memoryTF91:UITextField!
    @IBOutlet weak var memoryTF92:UITextField!
    @IBOutlet weak var memoryTF93:UITextField!
    @IBOutlet weak var memoryTF94:UITextField!
    @IBOutlet weak var memoryTF95:UITextField!
    @IBOutlet weak var memoryTF96:UITextField!
    @IBOutlet weak var memoryTF97:UITextField!
    @IBOutlet weak var memoryTF98:UITextField!
    @IBOutlet weak var memoryTF99:UITextField!
    
    @IBOutlet weak var instructionQueueTBV: UITableView!
    // function is used to display the respective text in the instruction decoder and instruction register depending on the slider value
    @IBAction func opcodeSlider(sender: UISlider) {
        
        opCodeInstructionTF.text = String(Int(sender.value))
        switch Int(sender.value) {
            
        case 0: opCodeInstructionTF.text = "0"
        decoder1TF.text = "COPY INPUT CARD INTO CELL"
        decoder4TF.text = "ADVANCE CARD"
            
        case 1:  opCodeInstructionTF.text = "1"
        decoder1TF.text = "ERASE ACCUMULATOR.COPY CONTENTS OF CELL"
        decoder4TF.text = "INTO ACCUMULATOR"
            
        case 2:  opCodeInstructionTF.text = "2"
        decoder1TF.text = "ADD CONTENT OF CELL"
        decoder4TF.text = "INTO ACCUMULATOR"
            
        case 3:  opCodeInstructionTF.text = "3"
        decoder1TF.text = "MOVE BUG TO CELL"
        decoder4TF.text = "."
            
        case 4:  opCodeInstructionTF.text = "4"
        decoder1TF.text = "SHIFT ACCUMULATOR LEFT"
        decoder4TF.text = "THEN RIGHT"
            
        case 5:  opCodeInstructionTF.text = "5"
        decoder1TF.text = "COPY CONTENTS OF CELL"
        decoder4TF.text = "ON OUTPUT CARD AND ADVANCE CARD."
            
        case 6:  opCodeInstructionTF.text = "6"
        decoder1TF.text = "COPY ACCUMULATOR INTO CELL"
        decoder4TF.text = "."
            
        case 7:  opCodeInstructionTF.text = "7"
        decoder1TF.text = "SUBTRACT CONTENTS OF CELL"
        decoder4TF.text = "FROM ACCUMULATOR."
            
        case 8:  opCodeInstructionTF.text = "8"
        decoder1TF.text = "WRITE BUGS CELL NO. IN CELL 99. MOVE BUG TO CELL"
        decoder4TF.text = "."
            
        case 9:  opCodeInstructionTF.text = "9"
        decoder1TF.text = "MOVE BUG TO CELL"
        decoder4TF.text = "STOP"
            
        default: break
            
        }
        
    }
    // to queue the instructions in instruction queue table entered in the instruction text field
    @IBAction func queueBTN(sender: AnyObject) {
        
        let temp = instructionInputTF.text!
        let instructionCharacters = temp.characters.map { String($0) }
        if temp.characters.count < 3 {
            let alert = UIAlertController(title: "Invalid Input", message: "Instruction should be 3 digit input", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if instructionCharacters[1] == "-" || instructionCharacters[2] == "-" {
            
            let alert = UIAlertController(title: "Invalid Input", message: "Instruction should be positive 3 digit input", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        else if Int(temp)! < 0 {
            let alert = UIAlertController(title: "Invalid Input", message: "Instruction should be positive 3 digit input", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        else {
            instructions.append(instructionInputTF.text!)
            instructionsStore.append(instructionInputTF.text!)
            instructionInputTF.text = ""
            instructionInputTF.backgroundColor = UIColor.whiteColor()
            instructionQueueTBV.reloadData()
        }
    }
    // load the instructions when load button is tapped.
    @IBAction func loadIRMemBTN(sender: AnyObject) {
        
        if instructions.count == 0 {
            let alert = UIAlertController(title: "Alert", message: "No instructions in the Queue to load", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else {
            
            var count = 0
            for i in  Int(pcTF.text!)!..<Int(pcTF.text!)!+instructions.count {
                memorycellTF[i].text = instructions[count]
                memorycellTF[i].backgroundColor = UIColor.yellowColor()
                count++
            }
            instructions.removeAll()
            instructionQueueTBV.reloadData()
        }
    }
    // this method is called when memorycellAddress1Slider is changes its value
    @IBAction func memorycellAddress1Slider(sender: UISlider) {
        memorycellAddress1TF.text = String(Int(sender.value))
        decoder2TF.text = String(Int(sender.value))
    }
    // this method is called when memorycellAddress2Slider is changes its value
    @IBAction func memorycellAddress2Slider(sender: UISlider) {
        memorycellAddress2TF.text = String(Int(sender.value))
        decoder3TF.text = String(Int(sender.value))
    }
    
   
    
    
    
    // reset button is used for reset all the fields in the manual mode
    @IBAction func resetAllBTN(sender: AnyObject) {
        let alert = UIAlertController(title: "Reset all the fields", message: "Do you want to continue?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler: {action in
            self.resetCode()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    // assign default values when the view is loaded.
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Manual Mode"
        resetCode()
        call()
        opCodeInstructionTF.enabled = false
        memorycellAddress1TF.enabled = false
        memorycellAddress1TF.enabled = false
        memorycellAddress2TF.enabled = false
        decoder1TF.enabled = false
        decoder2TF.enabled = false
        decoder3TF.enabled = false
        decoder4TF.enabled = false
        accumulatorTestTF.enabled = false
        opCodeInstructionTF.text = "0"
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action:Selector("dismissKeyboard")))
        instructionInputTF.keyboardType = UIKeyboardType.PhonePad
        outputTextView.keyboardType = UIKeyboardType.NumberPad
        instructionInputTF.delegate = self
        outputTextView.delegate = self
        pcTF.delegate = self
        accumulatorTF.delegate = self
        
        // Do any additional setup after loading the view.
    }
    // dismiss the keyboard
    func dismissKeyboard(){
        instructionInputTF.resignFirstResponder()
        for i in 0...99 {
            memorycellTF[i].resignFirstResponder()
        }
        accumulatorTF.resignFirstResponder()
        pcTF.resignFirstResponder()
        outputTextView.resignFirstResponder()
    }
    // function used to dismiss the keyboard when return is pressed on the keyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    // changes the background color of the active textfield
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.backgroundColor = UIColor.yellowColor()
        scrollView.setContentOffset(CGPointMake(0,textField.frame.origin.y-70), animated: true)
        
    }
    // changes the background color of the active textfield when editing is done
    func textFieldDidEndEditing(textField: UITextField) {
        textField.backgroundColor = UIColor.whiteColor()
        scrollView.setContentOffset(CGPointMake(0,-70), animated: true)
        dismissKeyboard()
    }
    // changes the background color of the active textfield when editing is started
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.backgroundColor = UIColor.yellowColor()
        textField.textColor = UIColor.blackColor()
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // this code is called when the user press continue when alert message is displayed when reset button is tapped
    func resetCode() {
        opCodeInstructionTF.text = "0"
        memorycellAddress1TF.text = "0"
        memorycellAddress2TF.text = "0"
        accumulatorTestTF.text = "IS INPUT CARD BLANK?"
        decoder1TF.text = "COPY INPUT CARD INTO CELL"
        decoder2TF.text = "0"
        decoder3TF.text = "0"
        decoder4TF.text = "ADVANCE CARD"
        pcTF.text = "00"
        accumulatorTF.text = "0000"
        instructionInputTF.text = ""
        instructions.removeAll()
        instructionQueueTBV.reloadData()
        opcodeSlider.value = 0
        memoryAddress1Slider.value = 0
        memoryAddress2Slider.value = 0
    }
    // assign empty to the all memorycells and assign them to the array of memorycellTF textfield array
    func call(){
        
        memoryTF0.text = " "
        memoryTF1.text = " "
        memoryTF2.text = " "
        memoryTF3.text = " "
        memoryTF4.text = " "
        memoryTF5.text = " "
        memoryTF6.text = " "
        memoryTF7.text = " "
        memoryTF8.text = " "
        memoryTF9.text = " "
        memoryTF10.text = " "
        memoryTF11.text = " "
        memoryTF12.text = " "
        memoryTF13.text = " "
        memoryTF14.text = " "
        memoryTF15.text = " "
        memoryTF16.text = " "
        memoryTF17.text = " "
        memoryTF18.text = " "
        memoryTF19.text = " "
        memoryTF20.text = " "
        memoryTF21.text = " "
        memoryTF22.text = " "
        memoryTF23.text = " "
        memoryTF24.text = " "
        memoryTF25.text = " "
        memoryTF26.text = " "
        memoryTF27.text = " "
        memoryTF28.text = " "
        memoryTF29.text = " "
        memoryTF30.text = " "
        memoryTF31.text = " "
        memoryTF32.text = " "
        memoryTF33.text = " "
        memoryTF34.text = " "
        memoryTF35.text = " "
        memoryTF36.text = " "
        memoryTF37.text = " "
        memoryTF38.text = " "
        memoryTF39.text = " "
        memoryTF40.text = " "
        memoryTF41.text = " "
        memoryTF42.text = " "
        memoryTF43.text = " "
        memoryTF44.text = " "
        memoryTF45.text = " "
        memoryTF46.text = " "
        memoryTF47.text = " "
        memoryTF48.text = " "
        memoryTF49.text = " "
        memoryTF50.text = " "
        memoryTF51.text = " "
        memoryTF52.text = " "
        memoryTF53.text = " "
        memoryTF54.text = " "
        memoryTF55.text = " "
        memoryTF56.text = " "
        memoryTF57.text = " "
        memoryTF58.text = " "
        memoryTF59.text = " "
        memoryTF60.text = " "
        memoryTF61.text = " "
        memoryTF62.text = " "
        memoryTF63.text = " "
        memoryTF64.text = " "
        memoryTF65.text = " "
        memoryTF66.text = " "
        memoryTF67.text = " "
        memoryTF68.text = " "
        memoryTF69.text = " "
        memoryTF70.text = " "
        memoryTF71.text = " "
        memoryTF72.text = " "
        memoryTF73.text = " "
        memoryTF74.text = " "
        memoryTF75.text = " "
        memoryTF76.text = " "
        memoryTF77.text = " "
        memoryTF78.text = " "
        memoryTF79.text = " "
        memoryTF80.text = " "
        memoryTF81.text = " "
        memoryTF82.text = " "
        memoryTF83.text = " "
        memoryTF84.text = " "
        memoryTF85.text = " "
        memoryTF86.text = " "
        memoryTF87.text = " "
        memoryTF88.text = " "
        memoryTF89.text = " "
        memoryTF90.text = " "
        memoryTF91.text = " "
        memoryTF92.text = " "
        memoryTF93.text = " "
        memoryTF94.text = " "
        memoryTF95.text = " "
        memoryTF96.text = " "
        memoryTF97.text = " "
        memoryTF98.text = " "
        memoryTF99.text = "8--"
        memorycellTF.append(memoryTF0)
        memorycellTF.append(memoryTF1)
        memorycellTF.append(memoryTF2)
        memorycellTF.append(memoryTF3)
        memorycellTF.append(memoryTF4)
        memorycellTF.append(memoryTF5)
        memorycellTF.append(memoryTF6)
        memorycellTF.append(memoryTF7)
        memorycellTF.append(memoryTF8)
        memorycellTF.append(memoryTF9)
        memorycellTF.append(memoryTF10)
        memorycellTF.append(memoryTF11)
        memorycellTF.append(memoryTF12)
        memorycellTF.append(memoryTF13)
        memorycellTF.append(memoryTF14)
        memorycellTF.append(memoryTF15)
        memorycellTF.append(memoryTF16)
        memorycellTF.append(memoryTF17)
        memorycellTF.append(memoryTF18)
        memorycellTF.append(memoryTF19)
        memorycellTF.append(memoryTF20)
        memorycellTF.append(memoryTF21)
        memorycellTF.append(memoryTF22)
        memorycellTF.append(memoryTF23)
        memorycellTF.append(memoryTF24)
        memorycellTF.append(memoryTF25)
        memorycellTF.append(memoryTF26)
        memorycellTF.append(memoryTF27)
        memorycellTF.append(memoryTF28)
        memorycellTF.append(memoryTF29)
        memorycellTF.append(memoryTF30)
        memorycellTF.append(memoryTF31)
        memorycellTF.append(memoryTF32)
        memorycellTF.append(memoryTF33)
        memorycellTF.append(memoryTF34)
        memorycellTF.append(memoryTF35)
        memorycellTF.append(memoryTF36)
        memorycellTF.append(memoryTF37)
        memorycellTF.append(memoryTF38)
        memorycellTF.append(memoryTF39)
        memorycellTF.append(memoryTF40)
        memorycellTF.append(memoryTF41)
        memorycellTF.append(memoryTF42)
        memorycellTF.append(memoryTF43)
        memorycellTF.append(memoryTF44)
        memorycellTF.append(memoryTF45)
        memorycellTF.append(memoryTF46)
        memorycellTF.append(memoryTF47)
        memorycellTF.append(memoryTF48)
        memorycellTF.append(memoryTF49)
        memorycellTF.append(memoryTF50)
        memorycellTF.append(memoryTF51)
        memorycellTF.append(memoryTF52)
        memorycellTF.append(memoryTF53)
        memorycellTF.append(memoryTF54)
        memorycellTF.append(memoryTF55)
        memorycellTF.append(memoryTF56)
        memorycellTF.append(memoryTF57)
        memorycellTF.append(memoryTF58)
        memorycellTF.append(memoryTF59)
        memorycellTF.append(memoryTF60)
        memorycellTF.append(memoryTF61)
        memorycellTF.append(memoryTF62)
        memorycellTF.append(memoryTF63)
        memorycellTF.append(memoryTF64)
        memorycellTF.append(memoryTF65)
        memorycellTF.append(memoryTF66)
        memorycellTF.append(memoryTF67)
        memorycellTF.append(memoryTF68)
        memorycellTF.append(memoryTF69)
        memorycellTF.append(memoryTF70)
        memorycellTF.append(memoryTF71)
        memorycellTF.append(memoryTF72)
        memorycellTF.append(memoryTF73)
        memorycellTF.append(memoryTF74)
        memorycellTF.append(memoryTF75)
        memorycellTF.append(memoryTF76)
        memorycellTF.append(memoryTF77)
        memorycellTF.append(memoryTF78)
        memorycellTF.append(memoryTF79)
        memorycellTF.append(memoryTF80)
        memorycellTF.append(memoryTF81)
        memorycellTF.append(memoryTF82)
        memorycellTF.append(memoryTF83)
        memorycellTF.append(memoryTF84)
        memorycellTF.append(memoryTF85)
        memorycellTF.append(memoryTF86)
        memorycellTF.append(memoryTF87)
        memorycellTF.append(memoryTF88)
        memorycellTF.append(memoryTF89)
        memorycellTF.append(memoryTF90)
        memorycellTF.append(memoryTF91)
        memorycellTF.append(memoryTF92)
        memorycellTF.append(memoryTF93)
        memorycellTF.append(memoryTF94)
        memorycellTF.append(memoryTF95)
        memorycellTF.append(memoryTF96)
        memorycellTF.append(memoryTF97)
        memorycellTF.append(memoryTF98)
        memorycellTF.append(memoryTF99)
        memorycellTF[0].text = "001"
        for i in 0...99 {
            
            memorycellTF[i].delegate = self
            memorycellTF[i].keyboardType = UIKeyboardType.PhonePad
        }
        
    }
    
    func textField(textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String) -> Bool {
            let maxLength = 3
            let currentString: NSString = textField.text!
            let newString: NSString =
            currentString.stringByReplacingCharactersInRange(range, withString: string)
            let inverseSet = NSCharacterSet(charactersInString:"0123456789-").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered = components.joinWithSeparator("")
            return  string == filtered && newString.length <= maxLength
            
    }
    // return number of rows in section for instructionQueue
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return instructions.count
    }
    // datasource for instructionQueue
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell?
        
        if tableView == self.instructionQueueTBV{
            
            cell = UITableViewCell(style: UITableViewCellStyle.Default,reuseIdentifier: "memory")
            cell!.textLabel?.text = instructions[indexPath.row]
        }
        
        return cell!
    }
    // return number of sections for instructionQueue
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }    
    
}
