//
//  ManualViewController.swift
//  Caridac Simulator App
//
//  Created by Thao,Cher-Xa on 3/17/16.
//  Copyright © 2016 Kancharla,Venkata Sravya. All rights reserved.
//

import UIKit

class ManualViewController: UIViewController {
    
    @IBOutlet weak var decoder1TF: UITextField!
    @IBOutlet weak var decoder2TF: UITextField!
    @IBOutlet weak var decoder3TF: UITextField!
    @IBOutlet weak var decoder4TF: UITextField!
    @IBOutlet weak var accumulatorTestTF: UITextField!
    @IBOutlet weak var instructionRegLBL: UITextField!
    @IBOutlet weak var memorycellAddress1LBL: UITextField!
    @IBOutlet weak var memorycellAddress2LBL: UITextField!
    
    @IBAction func loadTableTBL(sender: AnyObject) {
        
        let alertView:UIAlertView = UIAlertView(title: "Task is in progress",
            message: String(format:"loading data from input table to reader table"),
            delegate: nil, cancelButtonTitle: "OK")
        
        alertView.show()
        
    }
    
    @IBAction func stepBtn(sender: AnyObject) {
        
        //        let alertView:UIAlertView = UIAlertView(title: "Task is in progress",
        //            message: String(format:"step by step execution of the input"),
        //            delegate: nil, cancelButtonTitle: "OK")
        //        alertView.show()
        
        decoder1TF.text = "erase accumulator copy contents of cell"
        
    }
    
    
    @IBAction func resetBTN(sender: AnyObject) {
        let alertView:UIAlertView = UIAlertView(title: "Task is in progress",
            message: String(format:"reset all the fields present on the screen"),
            delegate: nil, cancelButtonTitle: "OK")
        alertView.show()
        
    }
    
    @IBAction func clearMemoryBTN(sender: AnyObject) {
        let alertView:UIAlertView = UIAlertView(title: "Task is in progress",
            message: String(format:"clears data present in the memory cells"),
            delegate: nil, cancelButtonTitle: "OK")
        alertView.show()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        instructionRegLBL.text = "0"
        memorycellAddress1LBL.text = "0"
        memorycellAddress2LBL.text = "0"
        accumulatorTestTF.text = "is Input card blank?"
        decoder1TF.text = "copy input card into cell"
        decoder2TF.text = "0"
        decoder3TF.text = "0"
        decoder4TF.text = "advance card"
        instructionRegLBL.enabled = false
        memorycellAddress1LBL.enabled = false
        memorycellAddress1LBL.enabled = false
        memorycellAddress2LBL.enabled = false
        decoder1TF.enabled = false
        decoder2TF.enabled = false
        decoder3TF.enabled = false
        decoder4TF.enabled = false
        accumulatorTestTF.enabled = false
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
