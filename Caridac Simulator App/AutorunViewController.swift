    //
    //  AutorunViewController.swift
    //  Caridac Simulator App
    //
    //  Created by Vasamsetti,Chandrasekhar on 3/16/16.
    //  Copyright © 2016 Kancharla,Venkata Sravya. All rights reserved.
    //
    
    // import packages
    import UIKit
    
    // Autorunviewcontroller is for autorunmode.
    class AutorunViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {
        // store the instructions enterd in the instruction text field
        var instructionStore:[String] = []
        // store the data entered in the data text field
        var data:[String] = []
        // memorycell array to store the memorycell textfields
        var memorycellTF:[UITextField] = []
        // assign default value for accumulator
        var accumulatorContent = 0000
        // to store the data to display in the output table
        var outputTableContent:[String] = []
        // to store the instructions entered in the instruction text field
        var irStore:[String] = []
        // to store the array of program counter values
        var programCounter1:[Int] = []
        // variable is used to iterate through the program counter values
        var count1 = 0
        // this variable is used when jump insturction is executed
        var jumpPC = 0
        // textfield for 1st digit of the accumulator
        @IBOutlet weak var accumulator1TF: UITextField!
        // textfield for 2nd digit of the accumulator
        @IBOutlet weak var accumulator2TF: UITextField!
        // textfield for 3rd digit of the accumulator
        @IBOutlet weak var accumulator3TF: UITextField!
        // textfield for 4th digit of the accumulator
        @IBOutlet weak var accumulator4TF: UITextField!
        // textfield is used to store the program counter value
        @IBOutlet weak var pcTF: UITextField!
        // accumulator test
        @IBOutlet weak var accumulatorTestLBL: UILabel!
        // accumulator label to display - NO
        @IBOutlet weak var accumulatorNegLBL: UILabel!
        // accumulator label to display - YES
        @IBOutlet weak var accumulatorPosLBL: UILabel!


        // textfield to take the instructions
        @IBOutlet weak var instructionTF: UITextField!
        // textfield to take the data
        @IBOutlet weak var dataTF: UITextField!
        // to display the accumulator sign
        @IBOutlet weak var accumlatorSignLBL: UILabel!
        // outlet of the UIScrollView
        @IBOutlet var scrollView: UIScrollView!
        // instruction queue table outlet
        @IBOutlet weak var instructionQueueTBV: UITableView!
        // data queue table outlet
        @IBOutlet weak var dataQueueTBV: UITableView!
        // to display the result of the instruction in output table
        @IBOutlet weak var outputTBV: UITableView!
        // outlets for memory cell textfields
        @IBOutlet weak var memoryTF0:UITextField!
        @IBOutlet weak var memoryTF1:UITextField!
        @IBOutlet weak var memoryTF2:UITextField!
        @IBOutlet weak var memoryTF3:UITextField!
        @IBOutlet weak var memoryTF4:UITextField!
        @IBOutlet weak var memoryTF5:UITextField!
        @IBOutlet weak var memoryTF6:UITextField!
        @IBOutlet weak var memoryTF7:UITextField!
        @IBOutlet weak var memoryTF8:UITextField!
        @IBOutlet weak var memoryTF9:UITextField!
        @IBOutlet weak var memoryTF10:UITextField!
        @IBOutlet weak var memoryTF11:UITextField!
        @IBOutlet weak var memoryTF12:UITextField!
        @IBOutlet weak var memoryTF13:UITextField!
        @IBOutlet weak var memoryTF14:UITextField!
        @IBOutlet weak var memoryTF15:UITextField!
        @IBOutlet weak var memoryTF16:UITextField!
        @IBOutlet weak var memoryTF17:UITextField!
        @IBOutlet weak var memoryTF18:UITextField!
        @IBOutlet weak var memoryTF19:UITextField!
        @IBOutlet weak var memoryTF20:UITextField!
        @IBOutlet weak var memoryTF21:UITextField!
        @IBOutlet weak var memoryTF22:UITextField!
        @IBOutlet weak var memoryTF23:UITextField!
        @IBOutlet weak var memoryTF24:UITextField!
        @IBOutlet weak var memoryTF25:UITextField!
        @IBOutlet weak var memoryTF26:UITextField!
        @IBOutlet weak var memoryTF27:UITextField!
        @IBOutlet weak var memoryTF28:UITextField!
        @IBOutlet weak var memoryTF29:UITextField!
        @IBOutlet weak var memoryTF30:UITextField!
        @IBOutlet weak var memoryTF31:UITextField!
        @IBOutlet weak var memoryTF32:UITextField!
        @IBOutlet weak var memoryTF33:UITextField!
        @IBOutlet weak var memoryTF34:UITextField!
        @IBOutlet weak var memoryTF35:UITextField!
        @IBOutlet weak var memoryTF36:UITextField!
        @IBOutlet weak var memoryTF37:UITextField!
        @IBOutlet weak var memoryTF38:UITextField!
        @IBOutlet weak var memoryTF39:UITextField!
        @IBOutlet weak var memoryTF40:UITextField!
        @IBOutlet weak var memoryTF41:UITextField!
        @IBOutlet weak var memoryTF42:UITextField!
        @IBOutlet weak var memoryTF43:UITextField!
        @IBOutlet weak var memoryTF44:UITextField!
        @IBOutlet weak var memoryTF45:UITextField!
        @IBOutlet weak var memoryTF46:UITextField!
        @IBOutlet weak var memoryTF47:UITextField!
        @IBOutlet weak var memoryTF48:UITextField!
        @IBOutlet weak var memoryTF49:UITextField!
        @IBOutlet weak var memoryTF50:UITextField!
        @IBOutlet weak var memoryTF51:UITextField!
        @IBOutlet weak var memoryTF52:UITextField!
        @IBOutlet weak var memoryTF53:UITextField!
        @IBOutlet weak var memoryTF54:UITextField!
        @IBOutlet weak var memoryTF55:UITextField!
        @IBOutlet weak var memoryTF56:UITextField!
        @IBOutlet weak var memoryTF57:UITextField!
        @IBOutlet weak var memoryTF58:UITextField!
        @IBOutlet weak var memoryTF59:UITextField!
        @IBOutlet weak var memoryTF60:UITextField!
        @IBOutlet weak var memoryTF61:UITextField!
        @IBOutlet weak var memoryTF62:UITextField!
        @IBOutlet weak var memoryTF63:UITextField!
        @IBOutlet weak var memoryTF64:UITextField!
        @IBOutlet weak var memoryTF65:UITextField!
        @IBOutlet weak var memoryTF66:UITextField!
        @IBOutlet weak var memoryTF67:UITextField!
        @IBOutlet weak var memoryTF68:UITextField!
        @IBOutlet weak var memoryTF69:UITextField!
        @IBOutlet weak var memoryTF70:UITextField!
        @IBOutlet weak var memoryTF71:UITextField!
        @IBOutlet weak var memoryTF72:UITextField!
        @IBOutlet weak var memoryTF73:UITextField!
        @IBOutlet weak var memoryTF74:UITextField!
        @IBOutlet weak var memoryTF75:UITextField!
        @IBOutlet weak var memoryTF76:UITextField!
        @IBOutlet weak var memoryTF77:UITextField!
        @IBOutlet weak var memoryTF78:UITextField!
        @IBOutlet weak var memoryTF79:UITextField!
        @IBOutlet weak var memoryTF80:UITextField!
        @IBOutlet weak var memoryTF81:UITextField!
        @IBOutlet weak var memoryTF82:UITextField!
        @IBOutlet weak var memoryTF83:UITextField!
        @IBOutlet weak var memoryTF84:UITextField!
        @IBOutlet weak var memoryTF85:UITextField!
        @IBOutlet weak var memoryTF86:UITextField!
        @IBOutlet weak var memoryTF87:UITextField!
        @IBOutlet weak var memoryTF88:UITextField!
        @IBOutlet weak var memoryTF89:UITextField!
        @IBOutlet weak var memoryTF90:UITextField!
        @IBOutlet weak var memoryTF91:UITextField!
        @IBOutlet weak var memoryTF92:UITextField!
        @IBOutlet weak var memoryTF93:UITextField!
        @IBOutlet weak var memoryTF94:UITextField!
        @IBOutlet weak var memoryTF95:UITextField!
        @IBOutlet weak var memoryTF96:UITextField!
        @IBOutlet weak var memoryTF97:UITextField!
        @IBOutlet weak var memoryTF98:UITextField!
        @IBOutlet weak var memoryTF99:UITextField!
        @IBOutlet weak var instructionRegLBL: UITextField!
        @IBOutlet weak var memorycellAddress1LBL: UITextField!
        @IBOutlet weak var memorycellAddress2LBL: UITextField!
        @IBOutlet weak var accumulatorTestTF: UITextField!
        @IBOutlet weak var decoder1TF: UITextField!
        @IBOutlet weak var decoder2TF: UITextField!
        @IBOutlet weak var decoder3TF: UITextField!
        @IBOutlet weak var decoder4TF: UITextField!
        @IBOutlet weak var step: UIButton!
        @IBOutlet weak var reset: UIButton!
        @IBOutlet weak var loadDataBTN: UIButton!
        @IBOutlet weak var loadBTN: UIButton!
        @IBOutlet weak var instructionQueueBTN: UIButton!
        
        // loads instructions entered in the instructionTF
        @IBAction func loadTableTBL(sender: AnyObject) {
            
            let temp = instructionTF.text!
            let letters = temp.characters.map { String($0) }
            
            if temp.characters.count < 3 {
                let alert = UIAlertController(title: "Invalid Input", message: "Instruction should be 3 digit input", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else if letters[1] == "-" || letters[2] == "-" {
                
                let alert = UIAlertController(title: "Invalid Input", message: "Instruction contained special characters", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            else if Int(temp)! < 0 {
                let alert = UIAlertController(title: "Invalid Input", message: "Instruction should be positive 3 digit input", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            else {
                
                instructionStore.append(instructionTF.text!)
                irStore.append(instructionTF.text!)
                instructionTF.text = ""
                instructionQueueTBV.reloadData()
                loadBTN.enabled = true
                
            }
            
        }
        // loads the data entered in the dataTF to dataLoadTable
        @IBAction func dataLoadBTN(sender: AnyObject) {
            
            if  dataTF.text!.characters.count == 0 {
                
                let alert = UIAlertController(title: "Invalid Input", message: "No input is detected", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                dataTF.backgroundColor = UIColor.redColor()
            }
            else {
                
                let dataCharactersArray = dataTF.text!.characters.map { String($0) }
                
                if dataTF.text!.characters.count>1 {
                    
                    if dataCharactersArray[1] == "-" {
                        
                        let alert = UIAlertController(title: "Invalid Data", message: "Too many minus symbols are not allowed in a number", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        dataTF.backgroundColor = UIColor.redColor()
                    }
                    else {
                        
                        if dataTF.text!.characters.count > 2 {
                            
                            if dataCharactersArray[2] == "-" {
                                
                                let alert = UIAlertController(title: "Invalid Data", message: "Too many minus symbols are not allowed in a number", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                                self.presentViewController(alert, animated: true, completion: nil)
                                dataTF.backgroundColor = UIColor.redColor()
                            }
                            else {
                                data.append((dataTF.text!))
                                dataTF.text = ""
                                dataQueueTBV.reloadData()
                                dataTF.backgroundColor = UIColor.whiteColor()
                            }
                        }
                        else {
                            data.append((dataTF.text!))
                            dataTF.text = ""
                            dataQueueTBV.reloadData()
                            dataTF.backgroundColor = UIColor.whiteColor()
                        }
                    }
                }
                else {
                    data.append((dataTF.text!))
                    dataTF.text = ""
                    dataQueueTBV.reloadData()
                    dataTF.backgroundColor = UIColor.whiteColor()
                }
            }
            
        }
        
        // Loads the data from the instruction queue to the memorycells based on the program counter value.
        @IBAction func loadIRMemBTN(sender: AnyObject) {
            
            if instructionStore.isEmpty {
                
                let alert = UIAlertController(title: "Alert", message: "No instructions to load in the QUEUE", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            else if pcTF.text?.characters.count == 0 {
                
                let alert = UIAlertController(title: "Alert", message: "Program Counter Value is missing", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                pcTF.backgroundColor = UIColor.redColor()
            }
            else {
                
                let letters = pcTF.text!.characters.map { String($0) }
                if pcTF.text?.characters.count > 2 {
                    let alert = UIAlertController(title: "Invalid Input", message: "Program Counter value should be less than 100", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    pcTF.backgroundColor = UIColor.redColor()
                    pcTF.text! = "0"
                }
                else if pcTF.text?.characters.count == 2 {
                    if letters[0] == "-" || letters[1] == "-" {
                        
                        let alert = UIAlertController(title: "Invalid Input", message: "Program Counter contained special characters", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                        pcTF.backgroundColor = UIColor.redColor()
                        pcTF.text! = "0"
                    }
                    else {
                        
                        if Int(pcTF.text!)!  >= 80 {
                            
                            let alert = UIAlertController(title: "Invalid Input", message: "Exceeded the memory cell range", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
                            pcTF.backgroundColor = UIColor.redColor()
                            pcTF.text! = "0"
                        }
                        count1 = 0
                        step.enabled = true
                        instructionQueueBTN.enabled = false
                        var count = 0
                        for i in  Int(pcTF.text!)!..<Int(pcTF.text!)!+instructionStore.count {
                            
                            memorycellTF[i].text = instructionStore[count]
                            programCounter1.append(i)
                            count++
                            pcTF.enabled = false
                            loadBTN.enabled = false
                        }
                        memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                    }
                }
                else {
                    count1 = 0
                    step.enabled = true
                    instructionQueueBTN.enabled = false
                    var count = 0
                    for i in  Int(pcTF.text!)!..<Int(pcTF.text!)!+instructionStore.count {
                        
                        memorycellTF[i].text = instructionStore[count]
                        programCounter1.append(i)
                        count++
                        pcTF.enabled = false
                        loadBTN.enabled = false
                    }
                    memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                }
            }
        }
        
        
        // function used to dismiss the keyboard when return is pressed on the keyboard
        func textFieldShouldReturn(textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
        // changes the background color of the active textfield
        func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
            textField.backgroundColor = UIColor.yellowColor()
            textField.textColor = UIColor.blackColor()
            return true
        }
        // moves up the textfield hidden by the keyboard
        func textFieldDidBeginEditing(textField: UITextField) {
            
            scrollView.setContentOffset(CGPointMake(0,260), animated: true)
        }
        // changes the background color of the active textfield when editing is done
        func textFieldDidEndEditing(textField: UITextField) {
            textField.backgroundColor = UIColor.whiteColor()
            scrollView.setContentOffset(CGPointMake(0,-60), animated: true)
            dismissKeyboard()
        }
        // function is used to limit the number of characters to enter in the instructionTF,dataTF and pcTF.
        func textField(textField: UITextField,shouldChangeCharactersInRange range: NSRange,replacementString string: String) -> Bool {
            
            let maxLength = 3
            let currentString: NSString = textField.text!
            let newString: NSString =
            currentString.stringByReplacingCharactersInRange(range, withString: string)
            let inverseSet = NSCharacterSet(charactersInString:"0123456789-").invertedSet
            let components = string.componentsSeparatedByCharactersInSet(inverseSet)
            let filtered = components.joinWithSeparator("")
            return  string == filtered && newString.length <= maxLength
        }
        // step button is used to execute the instruction one by one.
        @IBAction func stepBtn(sender: AnyObject) {
            
            if  memorycellTF[programCounter1[count1]].text?.characters.count == 0 {
                
                let alert = UIAlertController(title: "Alert", message: "No instructions to execute", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else {
                
                accumulatorTestTF.text = ""
                
                let temp = Int(instructionStore[0])!%100
                let decoder1 = "\(temp/10)"
                let decoder2 = "\(temp%10)"
                let choice:Int? = Int(instructionStore[0])!/100
                var programCounter:Int = programCounter1[count1]
                if programCounter < 10 {
                    pcTF.text = String(0) + String(programCounter)
                }
                else {
                    pcTF.text = String(programCounter)
                }
                switch choice! {
                    // OPCODE- 0 moves the data from the dataTBV to the address of the memory cell mentioned in the instruction
                case 0: instructionRegLBL.text = "0"
                accumulatorPosLBL.text = " "
                accumulatorNegLBL.text = " "
                decoder1TF.text = "COPY INPUT DATA INTO CELL"
                decoder2TF.text = decoder1
                decoder3TF.text = decoder2
                memorycellAddress1LBL.text = decoder1
                memorycellAddress2LBL.text = decoder2
                decoder4TF.text = "ADVANCE PROGRAM COUNTER"
                accumulatorTestTF.text = "IS INPUT DATA BLANK?"
                let decoder:Int = Int(decoder1 + decoder2)!
                colorChanger()
                if data.isEmpty {
                    memorycellTF[programCounter].backgroundColor = UIColor.redColor()
                    let alert = UIAlertController(title: "Instruction Execution Failed", message: "No data found in the data register", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    var temp = count1
                    if temp>=1 {
                        temp = count1 - 1
                        memorycellTF[programCounter1[temp]].backgroundColor = UIColor.whiteColor()
                    }
                }
                else {
                    
                    memorycellTF[decoder].text = String(data[0])
                    memorycellTF[decoder].backgroundColor = UIColor.grayColor()
                    memorycellTF[decoder].textColor = UIColor.blackColor()
                    data.removeAtIndex(0)
                    dataQueueTBV.reloadData()
                    instructionStore.removeAtIndex(0)
                    instructionQueueTBV.reloadData()
                    memorycellTF[programCounter].backgroundColor = UIColor.greenColor()
                    var temp = count1
                    if temp>=1 {
                        temp = count1 - 1
                        memorycellTF[programCounter1[temp]].backgroundColor = UIColor.whiteColor()
                    }
                    count1++
                    if(count1 < programCounter1.count) {
                        pcTF.text = String(programCounter1[count1])
                        memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                    }
                    else {
                        let alert = UIAlertController(title: "HALT", message: "All instructions are processed", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                if instructionStore.isEmpty {
                    step.enabled = false
                    }
                    
                    // OPCODE- 1 erase the accumulator and copy contents of cell to the accumulator
                case 1: instructionRegLBL.text = "1"
                decoder1TF.text = "ERASE ACCUMULATOR.COPY CONTENTS OF CELL"
                decoder2TF.text = decoder1
                decoder3TF.text = decoder2
                memorycellAddress1LBL.text = decoder1
                memorycellAddress2LBL.text = decoder2
                decoder4TF.text = "INTO ACCUMULATOR"
                accumulatorPosLBL.text = " "
                accumulatorNegLBL.text = " "
                colorChanger()
                let decoder:Int  = Int(decoder1 + decoder2)!
                for i in 0..<irStore.count{
                    let temporaryString = irStore[i].characters.map { String($0) }
                    if temporaryString[0] == "8" {
                        memorycellTF[programCounter1[i]].backgroundColor = UIColor.whiteColor()
                    }
                    
                }
                if memorycellTF[decoder].text == " " {
                    memorycellTF[decoder].backgroundColor = UIColor.redColor()
                    let alert = UIAlertController(title: "Invalid instruction sequence", message: "Data is not present in the memory cell"+decoder1+decoder2, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    accumulatorContent = Int(memorycellTF[decoder].text!)!
                    accumulatorContentPlaceHolder(accumulatorContent)
                    instructionStore.removeAtIndex(0)
                    instructionQueueTBV.reloadData()
                    memorycellTF[programCounter1[count1]].backgroundColor = UIColor.greenColor()
                    var temporaryStore = count1
                    if temporaryStore>=1 {
                        temporaryStore = count1 - 1
                        memorycellTF[programCounter1[temporaryStore]].backgroundColor = UIColor.whiteColor()
                    }
                    count1++
                    if(count1 < programCounter1.count) {
                        pcTF.text = String(programCounter1[count1])
                        memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                        
                    }
                    else {
                        let alert = UIAlertController(title: "HALT", message: "All instructions are processed", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                if instructionStore.isEmpty {
                    step.enabled = false
                    }
                    // OPCODE -2 add the contents of cell to the accumulator content
                case 2: instructionRegLBL.text = "2"
                decoder1TF.text = "ADD CONTENTS OF CELL"
                decoder2TF.text = decoder1
                decoder3TF.text = decoder2
                memorycellAddress1LBL.text = decoder1
                memorycellAddress2LBL.text = decoder2
                decoder4TF.text = "INTO ACCUMULATOR."
                accumulatorPosLBL.text = " "
                accumulatorNegLBL.text = " "
                accumulatorTestTF.text = "|| "
                colorChanger()
                let decoder = Int(decoder1 + decoder2)!
                if memorycellTF[decoder].text == " " {
                    memorycellTF[decoder].backgroundColor = UIColor.redColor()
                    let alert = UIAlertController(title: "Invalid instruction sequence", message: "No data exist in the specified memory cell", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    let temp1 = Int(memorycellTF[decoder].text!)! + accumulatorContent
                    accumulatorContent = temp1
                    accumulatorContentPlaceHolder(temp1)
                    instructionStore.removeAtIndex(0)
                    instructionQueueTBV.reloadData()
                    memorycellTF[programCounter1[count1]].backgroundColor = UIColor.greenColor()
                    var temporaryStore = count1
                    if temporaryStore>=1 {
                        temporaryStore = count1 - 1
                        memorycellTF[programCounter1[temporaryStore]].backgroundColor = UIColor.whiteColor()
                    }
                    count1++
                    if(count1 < programCounter1.count) {
                        pcTF.text = String(programCounter1[count1])
                        memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                    }
                    else {
                        let alert = UIAlertController(title: "HALT", message: "All instructions are processed", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                if instructionStore.isEmpty {
                    step.enabled = false
                    }
                    // OPCODE-3 ACCUMULATOR TEST
                case 3: instructionRegLBL.text = "3"
                var tempAddress = 0
                decoder1TF.text = "MOVE PC TO CELL"
                decoder2TF.text = decoder1
                decoder3TF.text = decoder2
                memorycellAddress1LBL.text = decoder1
                memorycellAddress2LBL.text = decoder2
                colorChanger()
                accumulatorTestTF.text = "---------------------------->"
                accumulatorTestLBL.text = "ACCUMULATOR TEST"
                decoder4TF.text = "."
                let decoder = Int(decoder1 + decoder2)!
                let tempInstructionStore = instructionStore
                instructionStore.removeAll()
                memorycellTF[programCounter1[count1]].backgroundColor = UIColor.greenColor()
                var temporaryStore = count1
                if temporaryStore >= 1 {
                    temporaryStore = count1 - 1
                    memorycellTF[programCounter1[temporaryStore]].backgroundColor = UIColor.whiteColor()
                }
                let p = count1
                if accumulatorContent < 0 {
                    accumulatorNegLBL.text = "- VE"
                    
                    for i in 0..<programCounter1.count {
                        if programCounter1[i] == decoder {
                            tempAddress = i
                            let x1 = memorycellTF[programCounter1[i]].text
                            instructionStore.append(x1!)
                            for k in i+1..<programCounter1.count {
                                let x2 = memorycellTF[programCounter1[k]].text
                                instructionStore.append(x2!)
                            }
                            break
                        }
                    }
                    count1 = tempAddress
                    pcTF.text = String(programCounter1[tempAddress])
                    memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                    
                }
                else {
                    accumulatorPosLBL.text = "+ VE"
                    instructionStore = tempInstructionStore
                    instructionStore.removeAtIndex(0)
                    count1 = p + 1
                    if(count1 < programCounter1.count) {
                        pcTF.text = String(programCounter1[count1])
                        memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                    }
                    else {
                        let alert = UIAlertController(title: "HALT", message: "All instructions are processed", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                instructionQueueTBV.reloadData()
                if instructionStore.isEmpty {
                    step.enabled = false
                    }
                   // OPCODE-4 SHIFTING THE DIGITS
                case 4: instructionRegLBL.text = "4"
                programCounter = Int(pcTF.text!)!
                decoder1TF.text = "SHIFT ACCUMULATOR LEFT "+decoder1+" PLACES"
                decoder2TF.text = ".."
                decoder3TF.text = ".."
                memorycellAddress1LBL.text = decoder1
                memorycellAddress2LBL.text = decoder2
                decoder4TF.text = "THEN RIGHT"+decoder2+"PLACES."
                accumulatorPosLBL.text = " "
                accumulatorNegLBL.text = " "
                colorChanger()
                var temporaryString:[String] = [accumulator1TF.text!] + [accumulator2TF.text!] + [accumulator3TF.text!] + [accumulator4TF.text!]
                for _ in 0..<Int(decoder1)! {
                    temporaryString = temporaryString + ["0"]
                    temporaryString.removeAtIndex(0)
                }
                for _ in 0..<Int(decoder2)! {
                    temporaryString = ["0"] + temporaryString
                    temporaryString.removeAtIndex(4)
                }
                accumulator1TF.text = temporaryString[0]
                accumulator2TF.text = temporaryString[1]
                accumulator3TF.text = temporaryString[2]
                accumulator4TF.text = temporaryString[3]
                accumulatorContent = Int(temporaryString[0]+temporaryString[1]+temporaryString[2]+temporaryString[3])!
                instructionStore.removeAtIndex(0)
                instructionQueueTBV.reloadData()
                memorycellTF[programCounter1[count1]].backgroundColor = UIColor.greenColor()
                var temporaryStore = count1
                if temporaryStore>=1 {
                    temporaryStore = count1 - 1
                    memorycellTF[programCounter1[temporaryStore]].backgroundColor = UIColor.whiteColor()
                }
                count1++
                if(count1 < programCounter1.count) {
                    pcTF.text = String(programCounter1[count1])
                    memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                }
                else {
                    let alert = UIAlertController(title: "HALT", message: "All instructions are processed", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                if instructionStore.isEmpty {
                    step.enabled = false
                    }
                    // OPCODE-5 OUPUT
                case 5: instructionRegLBL.text = "5"
                programCounter = Int(pcTF.text!)!
                decoder1TF.text = "COPY CONTENTS OF CELL"
                decoder2TF.text = decoder1
                decoder3TF.text = decoder2
                memorycellAddress1LBL.text = decoder1
                memorycellAddress2LBL.text = decoder2
                decoder4TF.text = "TO OUTPUT TABLE AND ADVANCE PC"
                accumulatorPosLBL.text = " "
                accumulatorNegLBL.text = " "
                colorChanger()
                let decoder:Int = Int(decoder1 + decoder2)!
                if memorycellTF[decoder].text == " " {
                    memorycellTF[decoder].backgroundColor = UIColor.redColor()
                    let alert = UIAlertController(title: "Invalid instruction sequence", message: "No data exist in the specified memory cell", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    outputTableContent.append(memorycellTF[decoder].text!)
                    outputTBV.reloadData()
                    memorycellTF[programCounter1[count1]].backgroundColor = UIColor.greenColor()
                    var temp = count1
                    if temp>=1 {
                        temp = count1 - 1
                        memorycellTF[programCounter1[temp]].backgroundColor = UIColor.whiteColor()
                    }
                    count1++
                    if(count1 < programCounter1.count) {
                        pcTF.text = String(programCounter1[count1])
                        memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                    }
                    else {
                        let alert = UIAlertController(title: "HALT", message: "All instructions are processed", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                }
                instructionStore.removeAtIndex(0)
                instructionQueueTBV.reloadData()
                if instructionStore.isEmpty {
                    step.enabled = false
                    }
                    // OPCODE-6 Copy accumulator content into the mentioned cell in the instruction
                case 6: instructionRegLBL.text = "6"
                programCounter = Int(pcTF.text!)!
                decoder1TF.text = "COPY ACCUMULATOR INTO CELL"
                decoder2TF.text = decoder1
                decoder3TF.text = decoder2
                memorycellAddress1LBL.text = decoder1
                memorycellAddress2LBL.text = decoder2
                decoder4TF.text = "."
                accumulatorPosLBL.text = " "
                accumulatorNegLBL.text = " "
                colorChanger()
                let decoder:Int = Int(decoder1 + decoder2)!
                memorycellTF[decoder].text = "\(accumulatorContent)"
                instructionStore.removeAtIndex(0)
                memorycellTF[programCounter1[count1]].backgroundColor = UIColor.greenColor()
                var temp = count1
                if temp>=1 {
                    temp = count1 - 1
                    memorycellTF[programCounter1[temp]].backgroundColor = UIColor.whiteColor()
                }
                count1++
                if(count1 < programCounter1.count) {
                    pcTF.text = String(programCounter1[count1])
                    memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                }
                else {
                    let alert = UIAlertController(title: "HALT", message: "All instructions are processed", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                instructionQueueTBV.reloadData()
                if instructionStore.isEmpty {
                    step.enabled = false
                }
                    // OPCODE-7 subtract the content of the cell from the accumulator
                case 7:  instructionRegLBL.text = "7"
                programCounter = Int(pcTF.text!)!
                decoder1TF.text = "SUBTRACT CONTENTS OF CELL"
                decoder2TF.text = decoder1
                decoder3TF.text = decoder2
                memorycellAddress1LBL.text = decoder1
                memorycellAddress2LBL.text = decoder2
                decoder4TF.text = "FROM ACCUMULATOR."
                accumulatorPosLBL.text = " "
                accumulatorNegLBL.text = " "
                colorChanger()
                
                let decoder:Int = Int(decoder1 + decoder2)!
                if memorycellTF[decoder].text == " " {
                    
                    let alert = UIAlertController(title: "Invalid insturction sequence", message: "No data exist in the specified memory cell", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    
                    accumulatorContent = accumulatorContent - Int(memorycellTF[decoder].text!)!
                    accumulatorContentPlaceHolder(accumulatorContent)
                    instructionStore.removeAtIndex(0)
                    instructionQueueTBV.reloadData()
                    memorycellTF[programCounter1[count1]].backgroundColor = UIColor.greenColor()
                    var temporaryStore = count1
                    if temporaryStore>=1 {
                        temporaryStore = count1 - 1
                        memorycellTF[programCounter1[temporaryStore]].backgroundColor = UIColor.whiteColor()
                    }
                    count1++
                    if(count1 < programCounter1.count) {
                        pcTF.text = String(programCounter1[count1])
                        memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                        
                    }
                    else {
                        
                        let alert = UIAlertController(title: "HALT", message: "All instructions are processed", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
                
                if instructionStore.isEmpty {
                    step.enabled = false
                    }
                    // OPCODE-8 write pc cell no. in 99: and move pc to cell
                case 8:  instructionRegLBL.text = "8"
                decoder1TF.text = "WRITE PC CELL NO.IN 99: MOVE PC TO CELL"
                decoder2TF.text = decoder1
                decoder3TF.text = decoder2
                memorycellAddress1LBL.text = decoder1
                memorycellAddress2LBL.text = decoder2
                decoder4TF.text = ""
                accumulatorPosLBL.text = " "
                accumulatorNegLBL.text = " "
                pcTF.text = decoder1 + decoder2
                let decoder = Int(decoder1 + decoder2)!
                memorycellTF[99].backgroundColor = UIColor.yellowColor()
                memorycellTF[99].text = "8"+decoder1+decoder2
                let x = instructionStore
                instructionStore.removeAll()
                for i in 0..<irStore.count {
                    
                    if memorycellTF[decoder].text! == irStore[i] {
                        let temporaryStore = i
                        for k in temporaryStore..<irStore.count-i+1{
                            
                            if irStore[k] == memorycellTF[99].text {
                                break
                            }
                            else {
                                
                                instructionStore.append(irStore[k])
                            }
                            
                        }
                        
                    }
                    
                }
                memorycellTF[programCounter1[count1]].backgroundColor = UIColor.greenColor()
                jumpPC = count1
                var k = count1
                if k>=1 {
                    k = count1 - 1
                    memorycellTF[programCounter1[k]].backgroundColor = UIColor.whiteColor()
                }
                instructionStore += x
                instructionQueueTBV.reloadData()
                for i in 0..<programCounter1.count{
                    
                    if programCounter1[i] == decoder {
                        count1 = i
                    }
                    
                }
                memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                if(count1 < programCounter1.count) {
                    pcTF.text = String(programCounter1[count1])
                    memorycellTF[programCounter1[count1]].backgroundColor = UIColor.yellowColor()
                    
                }
                else {
                    
                    let alert = UIAlertController(title: "HALT", message: "All instructions are processed", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                if instructionStore.isEmpty {
                    step.enabled = false
                }
                // OPCODE-9 Halt and Reset
                case 9:  instructionRegLBL.text = "9"
                programCounter = Int(pcTF.text!)!
                decoder1TF.text = "MOVE PC TO CELL"
                decoder2TF.text = decoder1
                decoder3TF.text = decoder2
                memorycellAddress1LBL.text = decoder1
                memorycellAddress2LBL.text = decoder2
                decoder4TF.text = "STOP."
                instructionStore.removeAtIndex(0)
                instructionQueueTBV.reloadData()
                colorChanger()
                memorycellTF[programCounter1[count1]].backgroundColor = UIColor.greenColor()
                var k = count1
                if k>=1 {
                    k = count1 - 1
                    memorycellTF[programCounter1[k]].backgroundColor = UIColor.whiteColor()
                }
                pcTF.text = "00"
                
                let alert = UIAlertController(title: "HALT AND RESET", message: "All instructions are processed", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                step.enabled = false
                loadDataBTN.enabled = false
                    break
                    
                default: break
                    
                }
            }
            
            
        }
        // Color changer function is called when jump instruction is executed
        func colorChanger() {
            
            for i in 0..<irStore.count{
                let irStoreArray = irStore[i].characters.map { String($0) }
                if irStoreArray[0] == "8" {
                    memorycellTF[programCounter1[i]].backgroundColor = UIColor.whiteColor()
                }
            }
        }
        
        // Splitting the accumulator content into single digits to place in the accumulator text fields
        func accumulatorContentPlaceHolder(var accumulator:Int) {
            
            if accumulator < 0 {
                accumlatorSignLBL.text = "-"
                accumulator = accumulator * -1
            }
            else {
                accumlatorSignLBL.text = "+"
            }
            
            if accumulator>=0 && accumulator < 100 {
                accumulator1TF.text = "0"
                accumulator2TF.text = "0"
                accumulator3TF.text = "\(accumulator/10)"
                accumulator4TF.text = "\(accumulator%10)"
            }
            else if accumulator >= 100 && accumulator < 1000 {
                accumulator1TF.text = "0"
                accumulator2TF.text = "\(accumulator/100)"
                let temp2 = accumulator%100
                accumulator3TF.text = "\(temp2/10)"
                accumulator4TF.text = "\(temp2%10)"
            }
            else {
                accumulator1TF.text = "\(accumulator/1000)"
                let temp2 = accumulator%1000
                accumulator2TF.text = "\(temp2/100)"
                let temp3 = temp2%100
                accumulator3TF.text = "\(temp3/10)"
                accumulator4TF.text = "\(temp3%10)"
            }
        }
        //function is used to dismiss the keyboard
        func dismissKeyboard(){
            
            instructionTF.resignFirstResponder()
            dataTF.resignFirstResponder()
        }
        // assign empty to the all memorycells and assign them to the array of memorycellTF textfield array
        func call(){
            memoryTF0.text = ""
            memoryTF1.text = ""
            memoryTF2.text = ""
            memoryTF3.text = ""
            memoryTF4.text = ""
            memoryTF5.text = ""
            memoryTF6.text = ""
            memoryTF7.text = ""
            memoryTF8.text = ""
            memoryTF9.text = ""
            memoryTF10.text = ""
            memoryTF11.text = ""
            memoryTF12.text = ""
            memoryTF13.text = ""
            memoryTF14.text = ""
            memoryTF15.text = ""
            memoryTF16.text = ""
            memoryTF17.text = ""
            memoryTF18.text = ""
            memoryTF19.text = ""
            memoryTF20.text = ""
            memoryTF21.text = ""
            memoryTF22.text = ""
            memoryTF23.text = ""
            memoryTF24.text = ""
            memoryTF25.text = ""
            memoryTF26.text = ""
            memoryTF27.text = ""
            memoryTF28.text = ""
            memoryTF29.text = ""
            memoryTF30.text = ""
            memoryTF31.text = ""
            memoryTF32.text = ""
            memoryTF33.text = ""
            memoryTF34.text = ""
            memoryTF35.text = ""
            memoryTF36.text = ""
            memoryTF37.text = ""
            memoryTF38.text = ""
            memoryTF39.text = ""
            memoryTF40.text = ""
            memoryTF41.text = ""
            memoryTF42.text = ""
            memoryTF43.text = ""
            memoryTF44.text = ""
            memoryTF45.text = ""
            memoryTF46.text = ""
            memoryTF47.text = ""
            memoryTF48.text = ""
            memoryTF49.text = ""
            memoryTF50.text = ""
            memoryTF51.text = ""
            memoryTF52.text = ""
            memoryTF53.text = ""
            memoryTF54.text = ""
            memoryTF55.text = ""
            memoryTF56.text = ""
            memoryTF57.text = ""
            memoryTF58.text = ""
            memoryTF59.text = ""
            memoryTF60.text = ""
            memoryTF61.text = ""
            memoryTF62.text = ""
            memoryTF63.text = ""
            memoryTF64.text = ""
            memoryTF65.text = ""
            memoryTF66.text = ""
            memoryTF67.text = ""
            memoryTF68.text = ""
            memoryTF69.text = ""
            memoryTF70.text = ""
            memoryTF71.text = ""
            memoryTF72.text = ""
            memoryTF73.text = ""
            memoryTF74.text = ""
            memoryTF75.text = ""
            memoryTF76.text = ""
            memoryTF77.text = ""
            memoryTF78.text = ""
            memoryTF79.text = ""
            memoryTF80.text = ""
            memoryTF81.text = ""
            memoryTF82.text = ""
            memoryTF83.text = ""
            memoryTF84.text = ""
            memoryTF85.text = ""
            memoryTF86.text = ""
            memoryTF87.text = ""
            memoryTF88.text = ""
            memoryTF89.text = ""
            memoryTF90.text = ""
            memoryTF91.text = ""
            memoryTF92.text = ""
            memoryTF93.text = ""
            memoryTF94.text = ""
            memoryTF95.text = ""
            memoryTF96.text = ""
            memoryTF97.text = ""
            memoryTF98.text = ""
            memoryTF99.text = "--"
            memorycellTF.append(memoryTF0)
            memorycellTF.append(memoryTF1)
            memorycellTF.append(memoryTF2)
            memorycellTF.append(memoryTF3)
            memorycellTF.append(memoryTF4)
            memorycellTF.append(memoryTF5)
            memorycellTF.append(memoryTF6)
            memorycellTF.append(memoryTF7)
            memorycellTF.append(memoryTF8)
            memorycellTF.append(memoryTF9)
            memorycellTF.append(memoryTF10)
            memorycellTF.append(memoryTF11)
            memorycellTF.append(memoryTF12)
            memorycellTF.append(memoryTF13)
            memorycellTF.append(memoryTF14)
            memorycellTF.append(memoryTF15)
            memorycellTF.append(memoryTF16)
            memorycellTF.append(memoryTF17)
            memorycellTF.append(memoryTF18)
            memorycellTF.append(memoryTF19)
            memorycellTF.append(memoryTF20)
            memorycellTF.append(memoryTF21)
            memorycellTF.append(memoryTF22)
            memorycellTF.append(memoryTF23)
            memorycellTF.append(memoryTF24)
            memorycellTF.append(memoryTF25)
            memorycellTF.append(memoryTF26)
            memorycellTF.append(memoryTF27)
            memorycellTF.append(memoryTF28)
            memorycellTF.append(memoryTF29)
            memorycellTF.append(memoryTF30)
            memorycellTF.append(memoryTF31)
            memorycellTF.append(memoryTF32)
            memorycellTF.append(memoryTF33)
            memorycellTF.append(memoryTF34)
            memorycellTF.append(memoryTF35)
            memorycellTF.append(memoryTF36)
            memorycellTF.append(memoryTF37)
            memorycellTF.append(memoryTF38)
            memorycellTF.append(memoryTF39)
            memorycellTF.append(memoryTF40)
            memorycellTF.append(memoryTF41)
            memorycellTF.append(memoryTF42)
            memorycellTF.append(memoryTF43)
            memorycellTF.append(memoryTF44)
            memorycellTF.append(memoryTF45)
            memorycellTF.append(memoryTF46)
            memorycellTF.append(memoryTF47)
            memorycellTF.append(memoryTF48)
            memorycellTF.append(memoryTF49)
            memorycellTF.append(memoryTF50)
            memorycellTF.append(memoryTF51)
            memorycellTF.append(memoryTF52)
            memorycellTF.append(memoryTF53)
            memorycellTF.append(memoryTF54)
            memorycellTF.append(memoryTF55)
            memorycellTF.append(memoryTF56)
            memorycellTF.append(memoryTF57)
            memorycellTF.append(memoryTF58)
            memorycellTF.append(memoryTF59)
            memorycellTF.append(memoryTF60)
            memorycellTF.append(memoryTF61)
            memorycellTF.append(memoryTF62)
            memorycellTF.append(memoryTF63)
            memorycellTF.append(memoryTF64)
            memorycellTF.append(memoryTF65)
            memorycellTF.append(memoryTF66)
            memorycellTF.append(memoryTF67)
            memorycellTF.append(memoryTF68)
            memorycellTF.append(memoryTF69)
            memorycellTF.append(memoryTF70)
            memorycellTF.append(memoryTF71)
            memorycellTF.append(memoryTF72)
            memorycellTF.append(memoryTF73)
            memorycellTF.append(memoryTF74)
            memorycellTF.append(memoryTF75)
            memorycellTF.append(memoryTF76)
            memorycellTF.append(memoryTF77)
            memorycellTF.append(memoryTF78)
            memorycellTF.append(memoryTF79)
            memorycellTF.append(memoryTF80)
            memorycellTF.append(memoryTF81)
            memorycellTF.append(memoryTF82)
            memorycellTF.append(memoryTF83)
            memorycellTF.append(memoryTF84)
            memorycellTF.append(memoryTF85)
            memorycellTF.append(memoryTF86)
            memorycellTF.append(memoryTF87)
            memorycellTF.append(memoryTF88)
            memorycellTF.append(memoryTF89)
            memorycellTF.append(memoryTF90)
            memorycellTF.append(memoryTF91)
            memorycellTF.append(memoryTF92)
            memorycellTF.append(memoryTF93)
            memorycellTF.append(memoryTF94)
            memorycellTF.append(memoryTF95)
            memorycellTF.append(memoryTF96)
            memorycellTF.append(memoryTF97)
            memorycellTF.append(memoryTF98)
            memorycellTF.append(memoryTF99)
            memorycellTF[0].text = "001"
            for i in 0..<100 {
                memorycellTF[i].enabled = false
            }
        }
        
        
        // reset button is used for reset all the fields in the autorun mode
        @IBAction func resetBTN(sender: AnyObject) {
            
            let alert = UIAlertController(title: "Reset all the fields", message: "Do you want to continue?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler: {action in
                self.resetCode()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        // this code is called when the user press continue when alert message is displayed when reset button is tapped
        func resetCode() {
            
            instructionRegLBL.text = "0"
            memorycellAddress1LBL.text = "0"
            memorycellAddress2LBL.text = "0"
            accumulatorTestTF.text = "IS INPUT CELL BLANK?"
            decoder1TF.text = "COPY INPUT CARD INTO CELL"
            decoder2TF.text = "0"
            decoder3TF.text = "0"
            decoder4TF.text = "ADVANCE PROGRAM COUNTER"
            for i in 1..<99 {
                memorycellTF[i].text = " "
                memorycellTF[i].backgroundColor = UIColor.whiteColor()
            }
            memorycellTF[99].text = "8--"
            memorycellTF[0].text = "001"
            memorycellTF[0].backgroundColor = UIColor.lightGrayColor()
            instructionStore.removeAll()
            instructionQueueTBV.reloadData()
            data.removeAll()
            dataQueueTBV.reloadData()
            outputTableContent.removeAll()
            outputTBV.reloadData()
            accumulator1TF.text = "0"
            accumulator2TF.text = "0"
            accumulator3TF.text = "0"
            accumulator4TF.text = "0"
            pcTF.text = "0"
            loadBTN.enabled = false
            instructionQueueBTN.enabled = true
            pcTF.enabled = true
            accumulatorTestLBL.text = "NO                      YES"
            accumulatorNegLBL.text = " "
            accumulatorPosLBL.text = " "
            loadDataBTN.enabled = true
            programCounter1.removeAll()
            irStore.removeAll()
            memorycellTF[0].backgroundColor = UIColor.greenColor()
        }
        // assign default values when the view is loaded.
        override func viewDidLoad() {
            super.viewDidLoad()
            self.navigationItem.title = "Autorun Mode"
            step.enabled = false
            call()
            resetCode()
            instructionRegLBL.enabled = false
            memorycellAddress1LBL.enabled = false
            memorycellAddress1LBL.enabled = false
            memorycellAddress2LBL.enabled = false
            decoder1TF.enabled = false
            decoder2TF.enabled = false
            decoder3TF.enabled = false
            decoder4TF.enabled = false
            accumulatorTestTF.enabled = false
            dataQueueTBV.registerClass(UITableViewCell.self, forCellReuseIdentifier: "memory")
            instructionQueueTBV.registerClass(UITableViewCell.self, forCellReuseIdentifier: "reader")
            accumulator1TF.text = ""
            accumulator2TF.text = ""
            accumulator3TF.text = ""
            accumulator4TF.text = ""
            pcTF.text = "00"
            pcTF.delegate = self
            dataTF.delegate = self
            instructionTF.delegate = self
            self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action:Selector("dismissKeyboard")))
            accumulator1TF.enabled = false
            accumulator2TF.enabled = false
            accumulator3TF.enabled = false
            accumulator4TF.enabled = false
            accumlatorSignLBL.text = ""
            scrollView.delegate = self
            instructionStore = []
            irStore = instructionStore
            // Do any additional setup after loading the view.
        }
        
        // return number of rows in section for dataQueue and instructionQueue and outputTable
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if tableView == self.dataQueueTBV {
                return data.count
            }
            else if tableView == self.instructionQueueTBV {
                return instructionStore.count
            }
            else {
                return outputTableContent.count
            }
        }
        // datasource for dataQueue and instructionQueue and outputTable
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            
            var cell:UITableViewCell?
            
            if tableView == self.dataQueueTBV{
                cell = UITableViewCell(style: UITableViewCellStyle.Default,reuseIdentifier: "memory")
                cell!.textLabel?.text = data[indexPath.row]
            }
            else if tableView == self.instructionQueueTBV {
                cell = UITableViewCell(style: UITableViewCellStyle.Default,reuseIdentifier: "reader")
                cell!.textLabel?.text = instructionStore[indexPath.row]
            }
            else {
                cell = UITableViewCell(style: UITableViewCellStyle.Default,reuseIdentifier: "output")
                cell!.textLabel?.text = outputTableContent[indexPath.row]
            }
            
            return cell!
        }
        // return number of sections for dataQueue and instructionQueue and outputTable
        func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return 1
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
    }
